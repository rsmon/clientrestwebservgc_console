package entites;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LigneDeCommande implements Serializable {
    
    private Long id;
    
    private Float    qteCom;
    private Commande laCommande;
   
    private Produit  leProduit;
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Float getQteCom() {
        return qteCom;
    }
    public void setQteCom(Float qteCom) {
        this.qteCom = qteCom;
    }
    public Commande getLaCommande() {
        return laCommande;
    }
    public void setLaCommande(Commande laCommande) {
        this.laCommande = laCommande;
    }
    public Produit getLeProduit() {
        return leProduit;
    }
    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }
    //</editor-fold>
}

package entites;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class Commande implements Serializable {
    
    private Long numCom;
    
    private Date dateCom;
    private String etatCom;
    
    private List<LigneDeCommande>lesLignesDeCommande=new LinkedList<LigneDeCommande>();
    
    private Client leClient;
    
    public void afficher(){
    
        System.out.printf("%6d %-10s %2s", numCom,dateCom,etatCom);
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    public Long getNumCom() {
        return numCom;
    }
    public void setNumCom(Long numCom) {
        this.numCom = numCom;
    }
    public Date getDateCom() {
        return dateCom;
    }
    public void setDateCom(Date dateCom) {
        this.dateCom = dateCom;
    }
    public String getEtatCom() {
        return etatCom;
    }
    public void setEtatCom(String etatCom) {
        this.etatCom = etatCom;
    }
    public List<LigneDeCommande> getLesLignesDeCommande() {
        return lesLignesDeCommande;
    }
    public void setLesLignesDeCommande(List<LigneDeCommande> lesLignesDeCommande) {
        this.lesLignesDeCommande = lesLignesDeCommande;
    }
    public Client getLeClient() {
        return leClient;
    }
    public void setLeClient(Client leClient) {
        this.leClient = leClient;
    }
    //</editor-fold>
}





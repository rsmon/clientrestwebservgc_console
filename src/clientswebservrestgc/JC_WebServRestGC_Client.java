
package clientswebservrestgc;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import dto.ResumeClient;
import entites.Client;
import java.util.List;

/**
 *
 * @author rsmon
 */

public class JC_WebServRestGC_Client {

   
 public static void main(String[] args) {
      
   JC_WebServiceRestGC jc= new JC_WebServiceRestGC();
    
   System.out.println("Chiffre d'affaires de l'année pour le client 101\n");
   System.out.println(jc.caClientAnneeenCours("101")+" €");
   System.out.println("\n");     
   
   ////////////////////////////////
      
   System.out.println("Informations du client 101\n");
  
   ClientResponse cr = jc.getLeClient_JSON(ClientResponse.class, "101");     
   Client          c = cr.getEntity(Client.class);
        
   System.out.println(c.getNomCli());      
   System.out.println(c.getLaRegion().getNomRegion());
   System.out.println("\n");      
   
   /////////////////////////////////
         
   ClientResponse crl=jc.getLesClients_JSON(ClientResponse.class);
         
   List<Client> lesClients= crl.getEntity(new GenericType<List<Client>>() {});
    
   System.out.println("Liste des clients avec leurs chiffres d'affaires\n");
   
   System.out.println("En utilisant un arbre ");
   
   for (Client cl: lesClients){
         
      System.out.printf(
                        "%-25s %10s €\n",
                        cl.getNomCli(),jc.caClientAnneeenCours(cl.getNumCli().toString())
      );
   }      
   System.out.println("\n");
 
   
   System.out.println("Liste des clients avec leurs chiffres d'affaires et soldes");
   
   System.out.println("En utilisant une dto ( données à plat)\n");
   
    ClientResponse cr2=jc.getTousLesResumeClients_JSON(ClientResponse.class);
    
    List<ResumeClient> lrc=cr2.getEntity(new GenericType<List<ResumeClient>>() {});
    
    
    for(ResumeClient rcli : lrc){
   
        System.out.printf(
                "%-25s %8.2f € %8.2f €\n",
                rcli.getNomCli(),rcli.getCaAnneeEnCours() ,rcli.getSolde());
    }
    System.out.println("\n");
   
 }

}

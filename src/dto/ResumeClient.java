
package dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResumeClient {
 
    private Long   numCli;
    private String nomCli;
    private String adrCli;
    
    private Float  caAnneeEnCours;
    private Float  solde;
    private String codereg;
    private String nomreg;

    //<editor-fold defaultstate="collapsed" desc="getters et setters">
    public Long getNumCli() {
        return numCli;
    }
    
    public void setNumCli(Long numCli) {
        this.numCli = numCli;
    }
    
    public String getNomCli() {
        return nomCli;
    }
    
    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }
    
    public String getAdrCli() {
        return adrCli;
    }
    
    public void setAdrCli(String adrCli) {
        this.adrCli = adrCli;
    }
    
    public Float getCaAnneeEnCours() {
        return caAnneeEnCours;
    }
    
    public void setCaAnneeEnCours(Float caAnneeEnCours) {
        this.caAnneeEnCours = caAnneeEnCours;
    }
    
    public Float getSolde() {
        return solde;
    }
    
    public void setSolde(Float solde) {
        this.solde = solde;
    }
    
    public String getCodereg() {
        return codereg;
    }
    
    public void setCodereg(String codereg) {
        this.codereg = codereg;
    }
    
    public String getNomreg() {
        return nomreg;
    }
    
    public void setNomreg(String nomreg) {
        this.nomreg = nomreg;
    }
    //</editor-fold>
}

package entites;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class Produit implements Serializable {
    
 
    private String refProd;
   
    private String desigProd;
    private Float  prixProd;
  
    private List<LigneDeCommande>lesLignesDeCommande=new LinkedList<LigneDeCommande>();
   
    private CategorieProduit laCategorie;
    
    public void afficher(){
    
        System.out.printf("%-10s  %-20s  %5.2f",refProd,desigProd,prixProd);
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS ET SETTERS">
    
    public String getRefProd() {
        return refProd;
    }
    public void   setRefProd(String refProd) {
        this.refProd = refProd;
    }
    public String getDesigProd() {
        return desigProd;
    }
    public void   setDesigProd(String desigProd) {
        this.desigProd = desigProd;
    }
    public List<LigneDeCommande> getLesLignesDeCommande() {
        return lesLignesDeCommande;
    }
    public void   setLesLignesDeCommande(List<LigneDeCommande> lesLignesDeCommande) {
        this.lesLignesDeCommande = lesLignesDeCommande;
    }
    public Float  getPrixProd() {
        return prixProd;
    }
    public void   setPrixProd(Float prixProd) {
        this.prixProd = prixProd;
    }
    public CategorieProduit getLaCategorie() {
        return laCategorie;
    }
    public void setLaCategorie(CategorieProduit laCategorie) {
        this.laCategorie = laCategorie;
    }
    
    //</editor-fold>

    
}
    

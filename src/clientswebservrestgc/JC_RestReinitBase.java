/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientswebservrestgc;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * Jersey REST client generated for REST resource:ServiceRestInitBase
 * [tests]<br>
 * USAGE:
 * <pre>
 *        JC_RestReinitBase client = new JC_RestReinitBase();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author rsmon
 */
public class JC_RestReinitBase {
    
    private WebResource webResource;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/WebAppliGC_TPS/rest";

    public JC_RestReinitBase() {
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = Client.create(config);
        webResource = client.resource(BASE_URI).path("tests");
    }

    public String reinitbase() throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path("jeudessais/reinitbase");
        return resource.get(String.class);
    }

    public void close() {
        client.destroy();
    }
    
}
